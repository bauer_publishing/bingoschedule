$(document).ready(function() {
  
  //fetch games via AJAX and add to array
  var games = new Object();
  var lastsid = '';
  var newdate = new Date();
  var lastdrawtime = newdate.toISOString();
  game_get(lastdrawtime);
  
  
  
  
    
  function game_get(drawtime){
    $.getJSON( "/bingoschedule/gameget/" + drawtime, function( response ) {
      // step through the JSON response and put it in the `games` array
      $.each( response, function( key, value ) {
        games.key = value;
        var thisgame = game_format(games.key);
        if ($('#bs-' + value.scheduleid).length == 0) {
          $('#bs-games').append(thisgame);
          lastdrawtime = value.drawtime;
        }
        
      });
    });
  }
  
  //function to format a game before it goes into the table
  function game_format(thisgame) {
    var drawtime = new Date(thisgame.drawtime);
    var now = new Date();
    var time_remaining = Math.round((drawtime - now) / 1000);
    var mins = Math.floor(time_remaining/60);
    var secs = (time_remaining % 60) + "";
    var prize = 0;
    if (thisgame.prizeinitialamount % 100 > 0) {
      prize = (thisgame.prizeinitialamount/100).toFixed(2);
    }
    else {
      prize = thisgame.prizeinitialamount/100;
    }
    if (thisgame.prizeinitialamount >= 100) {
      prize = '&pound;' + prize;
    }
    else {
      prize = prize + 'p';
    }
    while (secs.length < 2) secs = "0" + secs;
    var game_formatted = '<div class="bs-row" id="bs-' + thisgame.scheduleid + '" rel="' + thisgame.drawtime + '">';
    game_formatted += '<div class="bs-room bs-item">' + thisgame.clubname + '</div>';
    game_formatted += '<div class="bs-type bs-item">' + thisgame.ticketname + '</div>';
    game_formatted += '<div class="bs-price bs-item">' + thisgame.cardprice + 'p</div>';
    game_formatted += '<div class="bs-prize bs-item">' + prize + '</div>';
    game_formatted += '<div class="bs-time bs-item">' + mins + ':' + secs + '</div>';
    game_formatted += '<div class="clearer"></div>';
    game_formatted += '</div>';
    
    return game_formatted;
  }
  
  /**/
  // Function to update counters on all elements with class counter
  var doUpdate = function() {
    show_rows();
    $('.bs-row').each(function() {
      var drawtime = new Date($(this).attr('rel'));
      var now = new Date;
      var time_remaining = Math.round((drawtime - now) / 1000);
      var mins = Math.floor(time_remaining/60);
      var secs = (time_remaining % 60) + "";
      while (secs.length < 2) secs = "0" + secs;
      if (time_remaining < 90) {
        //remove this row
        $(this).remove();
        //alert($('.bs-row').length);
        show_rows();
        if ($('.bs-row').length < 16) {
          game_get(lastdrawtime);
        }
      }
      else {
        $(this).children('.bs-time').text(mins + ':' + secs);
      }
    });
  };
  
  /*
  function get_lastsid() {
    //get the number of elements
    var bs_num_games = $('.bs-row').length;
    //get the date of the last fetched game
    $('.bs-row').each(function (index, element) {
      if (index == bs_num_games - 1) {
        lastdraw8601 = $(this).attr('rel');
      }
    });
  }
  */
  
  function show_rows() {
    var counter = 0;
    $('.bs-row').each( function() {
      $(this).addClass('shown');
      counter++;
      if (counter == 10) {
        return false;
      }
    });
  }
  
  // Schedule the update to happen once every second
  setInterval(doUpdate, 1000);
});